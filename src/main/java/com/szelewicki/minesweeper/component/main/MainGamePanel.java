package com.szelewicki.minesweeper.component.main;

import com.szelewicki.minesweeper.component.api.ComponentActions;
import com.szelewicki.minesweeper.component.game.GameGridPanel;
import com.szelewicki.minesweeper.component.game.GamePanel;
import com.szelewicki.minesweeper.component.info.GameInfoPane;
import com.szelewicki.minesweeper.component.menu.GameMenuBar;
import com.szelewicki.minesweeper.config.AppSettings;
import javafx.scene.layout.VBox;

public class MainGamePanel extends VBox {

    private final GameMenuBar gameMenuBar;
    private final GameInfoPane gameInfoPane;
    private final GamePanel gamePanel;

    public MainGamePanel(ComponentActions componentActions, AppSettings appSettings) {
        this.gamePanel = new GamePanel();
        this.gameMenuBar = new GameMenuBar(componentActions, appSettings);
        this.gameInfoPane = new GameInfoPane(() -> {
            componentActions.reset();
            gamePanel.getGridPanel().reset();
        });
        gamePanel.getHighscoreView().setOnRestart(gameInfoPane::restart);
        getChildren().addAll(gameMenuBar, gameInfoPane, gamePanel);

        gameMenuBar.prefWidthProperty().bind(gamePanel.getGridPanel().widthProperty());
        gameInfoPane.prefWidthProperty().bind(gamePanel.getGridPanel().widthProperty());
    }

    public GameGridPanel getGridPanel() {
        return gamePanel.getGridPanel();
    }

    public GamePanel getGamePanel() {
        return gamePanel;
    }

    public void endGame(boolean won) {
        gameInfoPane.getTimer().stop();
        if (won) {
            gameInfoPane.winFace();
        } else {
            gameInfoPane.looseFace();
        }
    }

    public void startGame() {
        gameInfoPane.getTimer().start();
    }

    public GameInfoPane getGameInfoPane() {
        return gameInfoPane;
    }
}
