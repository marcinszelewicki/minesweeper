package com.szelewicki.minesweeper.component.main;

import com.szelewicki.minesweeper.component.api.ScoresGroup;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;
import javafx.stage.Stage;

import java.util.List;

public class MainView {

    private final Stage stage;
    private final MainGamePanel mainGamePanel;

    public MainView(Stage stage, MainGamePanel mainGamePanel) {
        this.stage = stage;
        this.mainGamePanel = mainGamePanel;
    }

    public Stage getStage() {
        return stage;
    }

    public void centerStage() {
        stage.sizeToScene();
        Rectangle2D bounds = Screen.getPrimary().getVisualBounds();
        stage.setX((bounds.getWidth() - stage.getWidth()) / 2);
        stage.setY((bounds.getHeight() - stage.getHeight()) / 2);
    }

    public MainGamePanel getMainGamePanel() {
        return mainGamePanel;
    }

    public void gameOver(boolean won) {
        mainGamePanel.endGame(won);
    }

    public void start() {
        mainGamePanel.startGame();
    }

    public void flagsLeft(int count) {
        mainGamePanel.getGameInfoPane().updateFlagsLeft(count);
    }

    public void fieldsCleared(int count) {
        mainGamePanel.getGameInfoPane().updateFieldsCleared(count);
    }

    public void setTotalFields(int totalFields) {
        mainGamePanel.getGameInfoPane().updateTotalFields(totalFields);
    }

    public long startTime() {
        return mainGamePanel.getGameInfoPane().getTimer().getGameStartMillis();
    }

    public long endTime() {
        return mainGamePanel.getGameInfoPane().getTimer().getGameEndMillis();
    }

    public void showHighscores(List<ScoresGroup> scoresGroups) {
        mainGamePanel.getGamePanel().showHighscores(scoresGroups);
    }

    public void hideHighscores() {
        mainGamePanel.getGamePanel().hideHighscores();
    }
}
