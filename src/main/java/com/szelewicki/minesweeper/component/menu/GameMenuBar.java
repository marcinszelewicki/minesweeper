package com.szelewicki.minesweeper.component.menu;

import com.szelewicki.minesweeper.component.api.ComponentActions;
import com.szelewicki.minesweeper.config.AppSettings;
import com.szelewicki.minesweeper.config.AppTranslations;
import com.szelewicki.minesweeper.config.GamePropertiesInfo;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;

import java.util.Arrays;

public class GameMenuBar extends MenuBar {

    private static final String LEVEL_DETAILS_FORMAT = " (%dx%d %d)";

    public GameMenuBar(ComponentActions componentActions, AppSettings appSettings) {
        getMenus().setAll(
                new Menu(AppTranslations.LEVEL, null,
                        item(AppTranslations.BEGINNER, levelDetails(appSettings.beginner()), componentActions::beginner),
                        item(AppTranslations.ADVANCED, levelDetails(appSettings.advanced()), componentActions::advanced),
                        item(AppTranslations.EXPERT, levelDetails(appSettings.expert()), componentActions::expert)
                ),
                new Menu(AppTranslations.THEME, null,
                        componentActions.themes().getThemes().stream()
                                .map(theme -> item(theme.getName(), () -> componentActions.themes().applyTheme(theme)))
                                .toArray(MenuItem[]::new)
                ),
                new Menu(AppTranslations.INFO, null,
                        item(AppTranslations.INFO_VALUE, () -> {
                        })));
    }

    private MenuItem item(SimpleStringProperty property, String textToAppend, Runnable... onAction) {
        MenuItem menuItem = new MenuItem();
        menuItem.textProperty().bind(property.concat(textToAppend));
        menuItem.setOnAction(e -> Arrays.stream(onAction).forEach(Runnable::run));
        return menuItem;
    }

    private MenuItem item(String text, Runnable onAction) {
        MenuItem menuItem = new MenuItem(text);
        menuItem.setOnAction(e -> onAction.run());
        return menuItem;
    }

    private String levelDetails(GamePropertiesInfo info) {
        return String.format(LEVEL_DETAILS_FORMAT, info.getCols(), info.getRows(), info.getMines());
    }
}
