package com.szelewicki.minesweeper.component.info;

import com.szelewicki.minesweeper.component.resources.Images;
import com.szelewicki.minesweeper.config.DefaultGameProperties;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;

public class GameInfoPane extends StackPane {

    private static final double RESTART_LABEL_WIDTH_DIVIDER = 2.2;

    private final Label minesLeft;
    private final CounterLabel restart;
    private final TimerLabel timer;
    private final Runnable restartAction;

    public GameInfoPane(Runnable restartAction) {
        this.restartAction = restartAction;

        minesLeft = new Label();
        minesLeft.setFont(Font.font(DefaultGameProperties.infoFontSize()));
        Images.FLAG.addListener((obs, o, n) -> minesLeft.setGraphic(new ImageView(n)));

        timer = new TimerLabel();

        restart = new CounterLabel();
        restart.cursorProperty().bind(Images.RELOAD_CURSOR);
        restart.setOnMousePressed(e -> restart());
        restart.prefWidthProperty().bind(this.widthProperty().divide(RESTART_LABEL_WIDTH_DIVIDER));
        restart.maxWidthProperty().bind(this.widthProperty().divide(RESTART_LABEL_WIDTH_DIVIDER));

        StackPane.setAlignment(minesLeft, Pos.CENTER_LEFT);
        StackPane.setAlignment(restart, Pos.CENTER);
        StackPane.setAlignment(timer, Pos.CENTER_RIGHT);

        getChildren().addAll(minesLeft, timer, restart);

        defaultFace();
    }

    public void restart() {
        timer.reset();
        defaultFace();
        restartAction.run();
    }

    public void updateFlagsLeft(int count) {
        minesLeft.setText("" + count);
    }

    public void updateFieldsCleared(int count) {
        restart.setTargetCount(count);
    }

    public void updateTotalFields(int totalFields) {
        restart.setTotal(totalFields);
    }

    public void defaultFace() {
        restart.setGraphics(Images.FACE_DEFAULT);
    }

    public void suspiciousFace() {
        restart.setGraphics(Images.FACE_SUSPICIOUS);
    }

    public void grinFace() {
        restart.setGraphics(Images.FACE_UNCERTAIN);
    }

    public void winFace() {
        restart.setGraphics(Images.FACE_WON);
    }

    public void looseFace() {
        restart.setGraphics(Images.FACE_LOST);
    }

    public TimerLabel getTimer() {
        return timer;
    }
}
