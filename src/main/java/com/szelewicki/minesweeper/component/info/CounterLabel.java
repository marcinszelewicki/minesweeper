package com.szelewicki.minesweeper.component.info;

import com.szelewicki.minesweeper.component.resources.Sounds;
import com.szelewicki.minesweeper.config.AppTranslations;
import com.szelewicki.minesweeper.config.DefaultGameProperties;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.beans.value.ObservableValue;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Font;
import javafx.util.Duration;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class CounterLabel extends StackPane {

    private static final double TITLE_FONT_SIZE = 14.0;

    private int lastCount;
    private final AtomicInteger total = new AtomicInteger(0);
    private Timeline timeline;
    private final Label face;
    private final Label counter;

    public CounterLabel() {
        Label title = new Label();
        title.setFont(Font.font(TITLE_FONT_SIZE));
        title.textProperty().bind(AppTranslations.COUNTER_TITLE);
        StackPane.setAlignment(title, Pos.TOP_CENTER);

        counter = new Label();
        counter.setFont(Font.font(DefaultGameProperties.counterFontSize()));

        face = new Label();
        face.setOpacity(0.8);

        getChildren().addAll(counter, face, title);
    }

    public void setTargetCount(int count) {
        if (count == 0) {
            return;
        }

        if (count - lastCount >= total.get()) {
            Optional.ofNullable(timeline).ifPresent(Timeline::stop);
            total.set(0);
            setCount(0);
            lastCount = 0;
            return;
        }

        timeline = new Timeline(20.0, new KeyFrame(
                Duration.millis(50.0), e -> setCount(total.decrementAndGet())
        ));
        timeline.setCycleCount(count - lastCount);

        lastCount = count;

        if (count > 0) {
            timeline.play();
        }

        Sounds.primaryReleased();
    }

    private void setCount(int count) {
        counter.setText("" + count);
    }

    public void setGraphics(ObservableValue<? extends Node> graphic) {
        face.graphicProperty().unbind();
        face.graphicProperty().bind(graphic);
    }

    public void setTotal(int totalFields) {
        Optional.ofNullable(timeline).ifPresent(Timeline::stop);
        total.set(totalFields);
        lastCount = 0;
        setCount(totalFields);
    }
}
