package com.szelewicki.minesweeper.component.info;

import com.szelewicki.minesweeper.component.resources.Images;
import com.szelewicki.minesweeper.config.DefaultGameProperties;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.util.Duration;

import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;

public class TimerLabel extends HBox {

    private static final int MAX_TIMER_ITERATIONS = 999;

    private final AtomicInteger secondsCounter = new AtomicInteger(0);
    private final Label text, icon;
    private long startMillis, stopMillis;
    private Timeline timeline;

    public TimerLabel() {
        text = new Label();
        text.setFont(Font.font(DefaultGameProperties.infoFontSize()));
        icon = new Label();
        Images.TIMER.addListener((obs, o, n) -> icon.setGraphic(new ImageView(n)));
        setAlignment(Pos.CENTER_RIGHT);
        getChildren().addAll(text, icon);
    }

    public void start() {
        timeline = new Timeline(new KeyFrame(Duration.seconds(1.0), e -> updateText()));
        timeline.setCycleCount(MAX_TIMER_ITERATIONS);
        timeline.play();
        startMillis = System.currentTimeMillis();
    }

    public void stop() {
        stopMillis = System.currentTimeMillis();
        timeline.stop();
    }

    public void reset() {
        secondsCounter.set(0);
        text.setText("");
        Optional.ofNullable(timeline).ifPresent(Timeline::stop);
    }

    public long getGameEndMillis() {
        return stopMillis;
    }

    public long getGameStartMillis() {
        return startMillis;
    }

    private void updateText() {
        text.setText("" + secondsCounter.incrementAndGet());
    }
}
