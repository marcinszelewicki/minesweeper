package com.szelewicki.minesweeper.component.resources;

import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.ImageCursor;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Images {

    //FACES
    public static final SimpleObjectProperty<ImageView> FACE_DEFAULT = new SimpleObjectProperty<>();
    public static final SimpleObjectProperty<ImageView> FACE_UNCERTAIN = new SimpleObjectProperty<>();
    public static final SimpleObjectProperty<ImageView> FACE_SUSPICIOUS = new SimpleObjectProperty<>();
    public static final SimpleObjectProperty<ImageView> FACE_WON = new SimpleObjectProperty<>();
    public static final SimpleObjectProperty<ImageView> FACE_LOST = new SimpleObjectProperty<>();

    //ICONS
    public static final Image ICON = ResourceLoader.loadImage("/img/ico128.png");
    public static final Image RELOAD = ResourceLoader.loadImage("/img/reload45.png");
    public static final SimpleObjectProperty<Image> TIMER = new SimpleObjectProperty<>();
    public static final SimpleObjectProperty<Image> MINE_NOT_DISCOVERED = new SimpleObjectProperty<>();
    public static final SimpleObjectProperty<Image> MINE_FOUND = new SimpleObjectProperty<>();
    public static final SimpleObjectProperty<Image> FLAG_MISPLACED = new SimpleObjectProperty<>();
    public static final SimpleObjectProperty<Image> FLAG = new SimpleObjectProperty<>();
    public static final SimpleObjectProperty<Image> QUESTION = new SimpleObjectProperty<>();

    //CURSORS
    public static final SimpleObjectProperty<ImageCursor> CURSOR = new SimpleObjectProperty<>();
    public static final SimpleObjectProperty<ImageCursor> RELOAD_CURSOR = new SimpleObjectProperty<>(new ImageCursor(RELOAD));
}
