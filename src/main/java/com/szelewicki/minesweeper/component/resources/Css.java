package com.szelewicki.minesweeper.component.resources;

import com.szelewicki.minesweeper.commons.SystemInfo;
import javafx.scene.Scene;

import java.net.URL;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class Css {

    public static void loadStyle(Scene node, String stylesheetPath) {
        List<String> stylesheets = Optional.ofNullable(Paths.get(SystemInfo.resolveAppRoot(stylesheetPath)))
                .map(ResourceLoader::readAllLines)
                .filter(list -> !list.isEmpty())
                .orElseGet(() -> loadFromResources(stylesheetPath));

        node.getStylesheets().setAll(stylesheets);
    }

    private static List<String> loadFromResources(String stylesheetPath) {
        return Optional.ofNullable(Css.class.getResource(stylesheetPath))
                .map(URL::toExternalForm)
                .map(Collections::singletonList)
                .orElse(Collections.emptyList());
    }
}
