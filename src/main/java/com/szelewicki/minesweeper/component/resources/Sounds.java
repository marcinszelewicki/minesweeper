package com.szelewicki.minesweeper.component.resources;

import com.szelewicki.minesweeper.commons.SystemInfo;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.media.AudioClip;

import java.util.Optional;

public class Sounds {
    public static final SimpleObjectProperty<AudioClip> PRIMARY_RELEASED = new SimpleObjectProperty<>();
    public static final SimpleObjectProperty<AudioClip> PRIMARY_PRESSED = new SimpleObjectProperty<>();
    public static final SimpleObjectProperty<AudioClip> SECONDARY_PRESSED = new SimpleObjectProperty<>();
    public static final SimpleObjectProperty<AudioClip> LOST = new SimpleObjectProperty<>();
    public static final SimpleObjectProperty<AudioClip> WON = new SimpleObjectProperty<>();

    public static AudioClip clip(String path, double unixVolume, double winVolume) {
        double volume = SystemInfo.isWin() ? winVolume : unixVolume;
        AudioClip clip = new AudioClip("" + Sounds.class.getResource(path));
        clip.setVolume(volume);
        return clip;
    }

    public static void primaryPressed() {
        play(PRIMARY_PRESSED);
    }

    public static void secondaryPressed() {
        play(SECONDARY_PRESSED);
    }

    public static void primaryReleased() {
        play(PRIMARY_RELEASED);
    }

    public static void lostSound() {
        play(LOST);
    }

    public static void winSound() {
        play(WON);
    }

    private static void play(SimpleObjectProperty<AudioClip> property) {
        Optional.ofNullable(property.get())
                .ifPresent(AudioClip::play);
    }
}
