package com.szelewicki.minesweeper.component.resources;

import com.szelewicki.minesweeper.commons.SystemInfo;
import javafx.scene.image.Image;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ResourceLoader {

    public static Image loadImage(String relativePath) {
        try {
            return new Image(SystemInfo.resolveAppRoot(relativePath));
        } catch (Exception e) {
        }
        return new Image(relativePath);
    }

    public static Image loadImage(String relativePath, double width, double height, boolean preserveRatio, boolean smooth) {
        try {
            return new Image(SystemInfo.resolveAppRoot(relativePath), width, height, preserveRatio, smooth);
        } catch (Exception e) {
        }
        return new Image(relativePath, width, height, preserveRatio, smooth);
    }

    public static List<String> loadFilesList(String relativePath, String... defaults) {
        return Optional.ofNullable(SystemInfo.resolveAppRoot(relativePath))
                .map(File::new)
                .map(File::listFiles)
                .map(files -> Arrays.stream(files).map(File::getName).collect(Collectors.toList()))
                .filter(list -> !list.isEmpty())
                .orElseGet(() -> Arrays.stream(defaults)
                        .map(relativePath::concat)
                        .collect(Collectors.toList()));
    }

    public static List<String> readAllLines(Path path) {
        try {
            return Files.readAllLines(path);
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }
}
