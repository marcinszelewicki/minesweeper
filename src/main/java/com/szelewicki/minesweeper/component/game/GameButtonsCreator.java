package com.szelewicki.minesweeper.component.game;

import com.szelewicki.minesweeper.commons.Iterations;

public class GameButtonsCreator {

    public static GameButton[][] create(int cols, int rows, double buttonSize) {
        GameButton[][] buttons = new GameButton[cols][rows];
        Iterations.forEach(cols, rows, (i, j) ->
                buttons[i][j] = new GameButton(i, j, buttonSize)
        );

        return buttons;
    }
}
