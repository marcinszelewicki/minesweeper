package com.szelewicki.minesweeper.component.game;

import com.szelewicki.minesweeper.commons.Iterations;
import javafx.scene.layout.GridPane;

public class GameGridPanel extends GridPane {

    private GameButton[][] buttons;

    public GameGridPanel() {
        setFocusTraversable(false);
    }

    public void setButtons(GameButton[][] buttons) {
        this.buttons = buttons;
        this.getChildren().clear();
        this.autosize();
        Iterations.deepStream(buttons).forEach(button ->
                this.add(button, button.getCol(), button.getRow(), 1, 1)
        );
    }

    public void reset() {
        Iterations.deepStream(buttons).forEach(GameButton::reset);
    }
}
