package com.szelewicki.minesweeper.component.game;

import com.szelewicki.minesweeper.commons.AppColors;
import com.szelewicki.minesweeper.component.api.ScoresGroup;
import com.szelewicki.minesweeper.component.api.ScoresItem;
import com.szelewicki.minesweeper.component.resources.Images;
import com.szelewicki.minesweeper.config.DefaultGameProperties;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.Font;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class HighscoreView extends VBox {

    private static final Background BACKGROUND = new Background(new BackgroundFill(AppColors.WHITE_OPAQUE.paint(),
            new CornerRadii(5.0), new Insets(-5.0)));
    private static final Border BORDER = new Border(new BorderStroke(AppColors.WHITE_OPAQUE.paint(), BorderStrokeStyle.SOLID,
            new CornerRadii(5.0), BorderStroke.THIN, new Insets(-5.0)));

    private Runnable onRestart;

    public HighscoreView() {
        setSpacing(25.0);
        setFillWidth(false);
        setAlignment(Pos.CENTER);
        cursorProperty().bind(Images.RELOAD_CURSOR);
        setOnMousePressed(e -> Optional.ofNullable(onRestart).ifPresent(Runnable::run));
    }

    public void setOnRestart(Runnable onRestart) {
        this.onRestart = onRestart;
    }

    public void showScores(List<ScoresGroup> scoresGroups) {
        List<VBox> scoreBoxes = scoresGroups.stream()
                .map(this::toBox)
                .collect(Collectors.toList());
        getChildren().addAll(scoreBoxes);
    }

    public void hideScores() {
        getChildren().clear();
    }

    private VBox toBox(ScoresGroup scoresGroup) {
        List<Label> labels = scoresGroup.getScores().stream()
                .map(score -> toScoreLabel(score, scoresGroup.getHighScorePosition() == scoresGroup.getScores().indexOf(score)))
                .collect(Collectors.toList());
        VBox vBox = new VBox();
        vBox.setAlignment(Pos.CENTER);
        vBox.setSpacing(5.0);
        vBox.setBackground(BACKGROUND);
        vBox.setBorder(BORDER);
        Label title = new Label(scoresGroup.getTitle());
        title.setFont(Font.font(DefaultGameProperties.highscoresFontSize() + 2.0));
        vBox.getChildren().add(title);
        vBox.getChildren().addAll(labels);
        return vBox;
    }

    private Label toScoreLabel(ScoresItem scoresItem, boolean highscore) {
        Label label = new Label(scoresItem.getSeconds() + System.lineSeparator() + scoresItem.getDate());
        label.setFont(Font.font(DefaultGameProperties.highscoresFontSize()));
        if (highscore) {
            label.setBackground(BACKGROUND);
            label.setBorder(BORDER);
            label.setTextFill(AppColors.DARK_BLUE.paint());
        }
        return label;
    }
}
