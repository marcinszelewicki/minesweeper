package com.szelewicki.minesweeper.component.game;

import com.szelewicki.minesweeper.component.resources.Images;
import com.szelewicki.minesweeper.component.resources.Sounds;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Cursor;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.paint.Paint;

import java.util.Optional;
import java.util.function.BiConsumer;

public class GameButton extends Button {

    private static final double FONT_SIZE_DIVIDER = 1.3;
    private static final String GAME_BUTTON_DYNAMIC_CSS_FORMAT = "-fx-label-padding: %d;-fx-font-size: %d";

    private final double size;
    private final SimpleObjectProperty<Image> imageObservable = new SimpleObjectProperty<>();
    private final int row, col;

    private boolean configured;
    private BiConsumer<Integer, Integer> primaryReleased, secondaryPressed;
    private Runnable defaultFace, suspiciousFace, grinFace;

    public GameButton(int col, int row, double size) {
        this.col = col;
        this.row = row;
        this.size = size;
        setFocusTraversable(false);
        size();

        int fontSize = (int) (size / FONT_SIZE_DIVIDER);
        int padding = (int) -(size / 2);
        setStyle(getStyle().concat(String.format(GAME_BUTTON_DYNAMIC_CSS_FORMAT, padding, fontSize)));

        cursorProperty().bind(Images.CURSOR);

        imageObservable.addListener((obs, o, n) -> {
            setGraphic(new ImageView(n));
            size();
        });

        this.setOnMouseEntered(me -> {
            defaultFace.run();
            if (!this.configured && !this.isDisabled() && !me.isPrimaryButtonDown() && !me.isSecondaryButtonDown()) {
                configureMouseActions();
            } else {
                setOnMouseReleased(e -> configureMouseActions());
            }
        });
    }

    public void reset() {
        this.configured = false;
    }

    public void setState(String text, boolean disabled, Paint paint, SimpleObjectProperty<Image> image) {

        setGraphic(null);
        setDisabled(disabled);
        imageObservable.unbind();
        Optional.ofNullable(image)
                .ifPresent(img -> {
                    setGraphic(new ImageView(image.get()));
                    imageObservable.bind(image);
                });

        setText(text);
        setTextFill(paint);

        if (disabled) {
            setOnMouseExited(null);
            setOnMousePressed(null);
            setOnMouseReleased(null);
            configured = true;
            cursorProperty().unbind();
            setCursor(Cursor.DEFAULT);
        } else if (!configured) {
            configureMouseActions();
            cursorProperty().bind(Images.CURSOR);
        } else {
            cursorProperty().bind(Images.CURSOR);
        }
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public GameButton setPrimaryReleased(BiConsumer<Integer, Integer> primaryReleased) {
        this.primaryReleased = primaryReleased;
        return this;
    }

    public GameButton setSecondaryPressed(BiConsumer<Integer, Integer> secondaryPressed) {
        this.secondaryPressed = secondaryPressed;
        return this;
    }

    public GameButton setDefaultFace(Runnable defaultFace) {
        this.defaultFace = defaultFace;
        return this;
    }

    public GameButton setSuspiciousFace(Runnable suspiciousFace) {
        this.suspiciousFace = suspiciousFace;
        return this;
    }

    public GameButton setGrinFace(Runnable grinFace) {
        this.grinFace = grinFace;
        return this;
    }

    private void configureMouseActions() {

        configured = true;

        this.setOnMousePressed(e -> {
            if (e.isPrimaryButtonDown()) {
                Sounds.primaryPressed();
                grinFace.run();
            } else {
                suspiciousFace.run();
                setOnMouseReleased(null);
                secondaryPressed.accept(col, row);
            }
        });

        this.setOnMouseReleased(e -> {
            primaryReleased.accept(col, row);
            configureMouseActions();
            setDisabled(true);
            defaultFace.run();
        });

        this.setOnMouseExited(e -> {
            setOnMousePressed(null);
            setOnMouseReleased(null);
            if (!this.isDisabled()) {
                configured = false;
            }
        });
    }

    private void size() {
        setPrefSize(size, size);
        setMaxSize(size, size);
        setMinSize(size, size);
    }
}
