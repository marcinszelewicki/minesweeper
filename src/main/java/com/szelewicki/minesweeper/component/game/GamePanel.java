package com.szelewicki.minesweeper.component.game;

import com.szelewicki.minesweeper.component.api.ScoresGroup;
import javafx.geometry.Pos;
import javafx.scene.layout.StackPane;

import java.util.List;

public class GamePanel extends StackPane {

    private final GameGridPanel gameGridPanel;
    private final HighscoreView highscoreView;

    public GamePanel() {
        gameGridPanel = new GameGridPanel();
        highscoreView = new HighscoreView();
        StackPane.setAlignment(gameGridPanel, Pos.CENTER);
        StackPane.setAlignment(highscoreView, Pos.CENTER);

        getChildren().addAll(highscoreView, gameGridPanel);
    }

    public void showHighscores(List<ScoresGroup> scoresGroups){
        highscoreView.showScores(scoresGroups);
        highscoreView.toFront();
    }

    public void hideHighscores(){
        highscoreView.hideScores();
        highscoreView.toBack();
    }

    public GameGridPanel getGridPanel() {
        return gameGridPanel;
    }

    public HighscoreView getHighscoreView() {
        return highscoreView;
    }
}
