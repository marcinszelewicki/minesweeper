package com.szelewicki.minesweeper.component.theme;

import com.szelewicki.minesweeper.commons.SystemInfo;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;
import java.util.Properties;

public class Theme {
    private final Properties properties;
    private final String name;

    public Theme(String path) {
        name = resolveNameFrom(path);
        properties = new Properties();
        try (InputStream is = Files.newInputStream(SystemInfo.applicationRootPath().resolve(path))) {
            properties.load(is);
        } catch (IOException e) {
            try {
                InputStream resourceAsStream = Theme.class.getResourceAsStream(path);
                properties.load(resourceAsStream);
            } catch (Exception ioe) {
                ioe.printStackTrace();
            }
        }

    }

    public String getName() {
        return name;
    }

    private String resolveNameFrom(String path) {
        String name = path.substring(path.lastIndexOf("/") + 1, path.indexOf("."));
        String firstLetter = name.substring(0, 1);
        return name.replaceFirst(firstLetter, firstLetter.toUpperCase());
    }

    public Optional<String> title() {
        return getProperty("title");
    }

    public Optional<String> counterName() {
        return getProperty("counter_name");
    }

    public Optional<String> beginner() {
        return getProperty("beginner");
    }

    public Optional<String> advanced() {
        return getProperty("advanced");
    }

    public Optional<String> expert() {
        return getProperty("expert");
    }

    public Optional<String> stylesheet() {
        return getProperty("stylesheet");
    }

    public Optional<String> cursor() {
        return getProperty("cursor");
    }

    public Optional<Double> faceWidth() {
        return getProperty("faceWidth").map(this::toDouble);
    }

    public Optional<Double> faceHeight() {
        return getProperty("faceHeight").map(this::toDouble);
    }

    public Optional<String> faceDefault() {
        return getProperty("face_default");
    }

    public Optional<String> faceUncertain() {
        return getProperty("face_uncertain");
    }

    public Optional<String> faceSuspicious() {
        return getProperty("face_suspicious");
    }

    public Optional<String> faceWon() {
        return getProperty("face_won");
    }

    public Optional<String> faceLost() {
        return getProperty("face_lost");
    }

    public Optional<String> mineNotDiscovered() {
        return getProperty("mine_not_discovered");
    }

    public Optional<String> mineFound() {
        return getProperty("mine_found");
    }

    public Optional<String> flagMisplaced() {
        return getProperty("flag_misplaced");
    }

    public Optional<String> flag() {
        return getProperty("flag");
    }

    public Optional<String> question() {
        return getProperty("question");
    }

    public Optional<String> timer() {
        return getProperty("timer");
    }

    public Optional<String> primaryPressed() {
        return getProperty("primary_pressed");
    }

    public Optional<Double> primaryPressedUnixvol() {
        return getProperty("primary_pressed_unix_vol").map(this::toDouble);
    }

    public Optional<Double> primaryPressedWinVol() {
        return getProperty("primary_pressed_win_vol").map(this::toDouble);
    }

    public Optional<String> primaryReleased() {
        return getProperty("primary_released");
    }

    public Optional<Double> primaryReleasedUnixVol() {
        return getProperty("primary_released_unix_vol").map(this::toDouble);
    }

    public Optional<Double> primaryReleasedWinVol() {
        return getProperty("primary_released_win_vol").map(this::toDouble);

    }

    public Optional<String> secondaryPressed() {
        return getProperty("secondary_pressed");
    }

    public Optional<Double> secondaryPressedUnixvol() {
        return getProperty("secondary_pressed_unix_vol").map(this::toDouble);
    }

    public Optional<Double> secondaryPressedWinVol() {
        return getProperty("secondary_pressed_win_vol").map(this::toDouble);
    }

    public Optional<String> lost() {
        return getProperty("lost");
    }

    public Optional<Double> lostUnixVol() {
        return getProperty("lost_unix_vol").map(this::toDouble);
    }

    public Optional<Double> lostWinVol() {
        return getProperty("lost_win_vol").map(this::toDouble);
    }

    public Optional<String> won() {
        return getProperty("won");
    }

    public Optional<Double> wonUnixVol() {
        return getProperty("won_unix_vol").map(this::toDouble);
    }

    public Optional<Double> wonWinVol() {
        return getProperty("won_win_vol").map(this::toDouble);
    }

    private Double toDouble(String s) {
        try {
            return Double.valueOf(s);
        } catch (Exception e) {
            return null;
        }
    }

    private Optional<String> getProperty(String cursor) {
        return Optional.ofNullable(properties.getProperty(cursor));
    }
}
