package com.szelewicki.minesweeper.component.theme;

import com.szelewicki.minesweeper.component.main.MainView;
import com.szelewicki.minesweeper.component.resources.Css;
import com.szelewicki.minesweeper.component.resources.Images;
import com.szelewicki.minesweeper.component.resources.ResourceLoader;
import com.szelewicki.minesweeper.component.resources.Sounds;
import com.szelewicki.minesweeper.config.AppTranslations;
import com.szelewicki.minesweeper.config.DefaultGameProperties;
import javafx.scene.ImageCursor;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public class Themes {

    private MainView mainView;
    private final List<Theme> themes;

    public Themes() {
        themes = ResourceLoader.loadFilesList(
                "/theme/", "necro.theme", "dark.theme", "light.theme")
                .stream()
                .map(Theme::new)
                .collect(Collectors.toList());
    }

    public void setMainView(MainView mainView) {
        this.mainView = mainView;
    }

    public List<Theme> getThemes() {
        return themes;
    }

    public void applyTheme(Theme theme) {
        theme.stylesheet().ifPresent(this::loadStyle);

        AppTranslations.TITLE.set(theme.title().orElse("Minesweeper"));
        AppTranslations.COUNTER_TITLE.set(theme.counterName().orElse("Fields to clear"));
        AppTranslations.BEGINNER.set(theme.beginner().orElse("Beginner"));
        AppTranslations.ADVANCED.set(theme.advanced().orElse("Advanced"));
        AppTranslations.EXPERT.set(theme.expert().orElse("Expert"));

        Images.CURSOR.set(theme.cursor().map(c -> new ImageCursor(image(c)))
                .orElse(null));

        double faceWidth = theme.faceWidth().orElse(48.0);
        double faceHeight = theme.faceHeight().orElse(48.0);
        Images.FACE_DEFAULT.set(theme.faceDefault().map(path -> imageView(path, faceWidth, faceHeight))
                .orElse(null));
        Images.FACE_UNCERTAIN.set(theme.faceUncertain().map(path -> imageView(path, faceWidth, faceHeight))
                .orElse(null));
        Images.FACE_SUSPICIOUS.set(theme.faceSuspicious().map(path -> imageView(path, faceWidth, faceHeight))
                .orElse(null));
        Images.FACE_WON.set(theme.faceWon().map(path -> imageView(path, faceWidth, faceHeight))
                .orElse(null));
        Images.FACE_LOST.set(theme.faceLost().map(path -> imageView(path, faceWidth, faceHeight))
                .orElse(null));

        Images.MINE_NOT_DISCOVERED.set(theme.mineNotDiscovered().map(this::image)
                .orElseGet(() -> image("/img/bomb35.png")));
        Images.MINE_FOUND.set(theme.mineFound().map(this::image)
                .orElseGet(() -> image("/img/boom35.png")));
        Images.FLAG_MISPLACED.set(theme.flagMisplaced().map(this::image)
                .orElseGet(() -> image("/img/flagredcrossed35.png")));
        Images.FLAG.set(theme.flag().map(this::image)
                .orElseGet(() -> image("/img/flagred35.png")));
        Images.QUESTION.set(theme.question().map(this::image)
                .orElseGet(() -> image("/img/question35.png")));
        Images.TIMER.set(theme.timer().map(this::image)
                .orElse(null));

        Sounds.PRIMARY_RELEASED.set(theme.primaryReleased()
                .map(path -> Sounds.clip(path,
                        theme.primaryReleasedUnixVol().orElse(0.04), theme.primaryReleasedWinVol().orElse(0.02)))
                .orElse(null));
        Sounds.PRIMARY_PRESSED.set(theme.primaryPressed()
                .map(path -> Sounds.clip(path,
                        theme.primaryPressedUnixvol().orElse(0.1), theme.primaryPressedWinVol().orElse(0.05)))
                .orElse(null));
        Sounds.SECONDARY_PRESSED.set(theme.secondaryPressed()
                .map(path -> Sounds.clip(path,
                        theme.secondaryPressedUnixvol().orElse(0.05), theme.secondaryPressedWinVol().orElse(0.02)))
                .orElse(null));

        Sounds.LOST.set(theme.lost()
                .map(path -> Sounds.clip(path,
                        theme.lostUnixVol().orElse(0.3), theme.lostWinVol().orElse(0.15)))
                .orElse(null));
        Sounds.WON.set(theme.won()
                .map(path -> Sounds.clip(path,
                        theme.wonUnixVol().orElse(0.3), theme.wonWinVol().orElse(0.15)))
                .orElse(null));
    }

    public void randomize() {
        Theme theme = themes.get(new Random().nextInt(themes.size()));
        applyTheme(theme);
    }

    private ImageView imageView(String path, double width, double height) {
        return new ImageView(ResourceLoader.loadImage(path, width, height, true, true));
    }

    private Image image(String path) {
        return ResourceLoader.loadImage(
                path, DefaultGameProperties.buttonSize(), DefaultGameProperties.buttonSize(), true, true);
    }

    private void loadStyle(String path) {
        Optional.ofNullable(mainView)
                .map(MainView::getStage)
                .map(Stage::getScene)
                .ifPresent(scene -> Css.loadStyle(mainView.getStage().getScene(), path));
    }
}
