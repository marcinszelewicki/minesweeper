package com.szelewicki.minesweeper.component.api;

import com.szelewicki.minesweeper.component.theme.Themes;

public interface ComponentActions {

    void onWindowClosed();

    void reset();

    void beginner();

    void advanced();

    void expert();

    Themes themes();
}
