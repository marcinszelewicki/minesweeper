package com.szelewicki.minesweeper.component.api;

import com.szelewicki.minesweeper.component.main.MainGamePanel;
import com.szelewicki.minesweeper.component.main.MainView;
import com.szelewicki.minesweeper.component.resources.Images;
import com.szelewicki.minesweeper.config.AppSettings;
import com.szelewicki.minesweeper.config.AppTranslations;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class AppComponents {

    private final MainView mainView;

    public AppComponents(Stage primaryStage, ComponentActions actions, AppSettings appSettings) {
        configureStage(primaryStage, actions::onWindowClosed);
        MainGamePanel mainGamePanel = new MainGamePanel(actions, appSettings);
        primaryStage.setScene(new Scene(mainGamePanel));
        this.mainView = new MainView(primaryStage, mainGamePanel);
    }

    public MainView getMainView() {
        return mainView;
    }

    private void configureStage(Stage primaryStage, Runnable onWindowClose) {

        primaryStage.titleProperty().bind(AppTranslations.TITLE);
        primaryStage.getIcons().add(Images.ICON);
        primaryStage.setResizable(false);
        primaryStage.setOnCloseRequest(e -> onWindowClose.run());
    }
}
