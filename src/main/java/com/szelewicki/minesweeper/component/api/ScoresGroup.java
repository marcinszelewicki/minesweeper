package com.szelewicki.minesweeper.component.api;

import java.util.List;

public class ScoresGroup {

    private final String title;
    private final List<ScoresItem> scores;
    private final int highScorePosition;

    public ScoresGroup(String title, List<ScoresItem> scores, int highScorePosition) {
        this.title = title;
        this.scores = scores;
        this.highScorePosition = highScorePosition;
    }

    public String getTitle() {
        return title;
    }

    public List<ScoresItem> getScores() {
        return scores;
    }

    public int getHighScorePosition() {
        return highScorePosition;
    }
}
