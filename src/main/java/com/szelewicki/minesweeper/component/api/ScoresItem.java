package com.szelewicki.minesweeper.component.api;

public class ScoresItem {

    private final String seconds;
    private final String date;

    public ScoresItem(String seconds, String date) {
        this.seconds = seconds;
        this.date = date;
    }

    public String getSeconds() {
        return seconds;
    }

    public String getDate() {
        return date;
    }
}
