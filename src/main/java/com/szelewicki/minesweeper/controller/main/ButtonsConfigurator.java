package com.szelewicki.minesweeper.controller.main;

import com.szelewicki.minesweeper.commons.Iterations;
import com.szelewicki.minesweeper.component.game.GameButton;
import com.szelewicki.minesweeper.component.game.GameButtonsCreator;
import com.szelewicki.minesweeper.config.GamePropertiesInfo;

public class ButtonsConfigurator {

    private final MainController controller;

    public ButtonsConfigurator(MainController controller) {
        this.controller = controller;
    }

    GameButton[][] newConfiguration(GamePropertiesInfo info) {

        GameButton[][] buttons = GameButtonsCreator.create(info.getCols(), info.getRows(), info.getButtonSize());

        Iterations.deepStream(buttons).forEach(button -> {
            button
                    .setPrimaryReleased((col, row) -> {
                        if (!controller.gameOver) {
                            controller.resolveStarted(col, row);
                            controller.service.onPrimaryReleased(col, row);
                        }
                    })
                    .setSecondaryPressed((col, row) -> {
                        if (!controller.gameOver) {
                            controller.resolveStarted(col, row);
                            controller.service.onSecondaryPressed(col, row);
                        }
                    })
                    .setDefaultFace(() -> {
                        if (!controller.gameOver) {
                            controller.components.getMainView().getMainGamePanel().getGameInfoPane().defaultFace();
                        }
                    })
                    .setSuspiciousFace(() -> {
                        if (!controller.gameOver) {
                            controller.components.getMainView().getMainGamePanel().getGameInfoPane().suspiciousFace();
                        }
                    })
                    .setGrinFace(() -> {
                        if (!controller.gameOver) {
                            controller.components.getMainView().getMainGamePanel().getGameInfoPane().grinFace();
                        }
                    });
        });

        return buttons;
    }
}
