package com.szelewicki.minesweeper.controller.main;

import com.szelewicki.minesweeper.commons.FieldState;
import com.szelewicki.minesweeper.commons.Iterations;
import com.szelewicki.minesweeper.component.theme.Themes;
import com.szelewicki.minesweeper.component.api.AppComponents;
import com.szelewicki.minesweeper.component.game.GameButton;
import com.szelewicki.minesweeper.config.AppSettings;
import com.szelewicki.minesweeper.config.GamePropertiesInfo;
import com.szelewicki.minesweeper.controller.highscore.ScoreHandler;
import com.szelewicki.minesweeper.controller.observable.FieldsButtonsConnector;
import com.szelewicki.minesweeper.controller.observable.ObservableField;
import com.szelewicki.minesweeper.controller.observable.ObservableFieldCreator;
import com.szelewicki.minesweeper.service.api.AppService;
import com.szelewicki.minesweeper.service.api.FieldInfo;
import com.szelewicki.minesweeper.service.fields.FieldInfoProvider;
import com.szelewicki.minesweeper.service.fields.InitiallyEmptyPoints;
import com.szelewicki.minesweeper.service.fields.MinesRandomizer;
import javafx.stage.Stage;

import java.util.function.Consumer;

public class MainController {

    final Themes themes;
    final AppSettings settings;
    final AppComponents components;
    final AppService service;
    final ScoreHandler scoreHandler;
    private final ButtonsConfigurator buttonsConfigurator;

    GamePropertiesInfo info;
    boolean notStarted;
    boolean gameOver;
    int currentLevel;
    private ObservableField[][] observableFields;


    public MainController(Stage primaryStage, Consumer<Themes> theme) {
        this.settings = new AppSettings();
        this.themes = new Themes();
        this.scoreHandler = new ScoreHandler();
        this.components = new AppComponents(primaryStage, ComponentActionsProvider.provide(this), settings);
        themes.setMainView(components.getMainView());
        components.getMainView().getStage().show();

        this.buttonsConfigurator = new ButtonsConfigurator(this);
        this.service = new AppService();
        new ActionServiceConfigurator(this).configure();

        theme.accept(themes);
    }

    public void setNewGrid(GamePropertiesInfo info) {
        this.info = info;
        this.observableFields = ObservableFieldCreator.create(info.getCols(), info.getRows());
        this.currentLevel = info.getLevel();

        service.setObservableFields(observableFields);
        resetFields();
        GameButton[][] buttons = buttonsConfigurator.newConfiguration(info);
        FieldsButtonsConnector.connect(buttons, observableFields);

        components.getMainView().getMainGamePanel().getGridPanel().setButtons(buttons);
        components.getMainView().getMainGamePanel().getGameInfoPane().getTimer().reset();
        components.getMainView().centerStage();
    }

    void resolveStarted(Integer col, Integer row) {
        if (notStarted && !gameOver) {
            notStarted = false;
            components.getMainView().start();
            service.setFields(createFieldInfos(col, row));
        }
    }

    void resetFields() {
        gameOver = false;
        notStarted = true;
        Iterations.deepStream(observableFields).forEach(field -> field.setFieldState(FieldState.INITIAL));
        service.updateFlagsCount();
        updateTotalCount();
        components.getMainView().hideHighscores();
    }

    void updateFieldsCleared() {
        components.getMainView().fieldsCleared(service.getClearedCount());
    }

    private void updateTotalCount() {
        components.getMainView().setTotalFields(info.getCols() * info.getRows() - info.getMines());
    }

    private FieldInfo[][] createFieldInfos(int col, int row) {
        boolean[][] mines = MinesRandomizer.randomize(info.getCols(), info.getRows(), info.getMines(), new InitiallyEmptyPoints(col, row));
        return FieldInfoProvider.provide(mines);
    }
}
