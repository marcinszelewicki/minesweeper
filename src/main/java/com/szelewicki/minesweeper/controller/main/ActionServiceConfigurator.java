package com.szelewicki.minesweeper.controller.main;

import com.szelewicki.minesweeper.component.api.ScoresGroup;
import com.szelewicki.minesweeper.component.resources.Sounds;
import com.szelewicki.minesweeper.controller.highscore.Score;
import com.szelewicki.minesweeper.controller.highscore.ScoreType;
import com.szelewicki.minesweeper.controller.highscore.Scores;

import java.util.List;

public class ActionServiceConfigurator {

    private final MainController controller;

    ActionServiceConfigurator(MainController controller) {
        this.controller = controller;
    }

    void configure() {
        controller.service
                .setOnGameOver(gameResult -> {

                    if (gameResult) {
                        controller.updateFieldsCleared();
                        Sounds.winSound();
                    } else {
                        Sounds.lostSound();
                    }

                    controller.components.getMainView().gameOver(gameResult);
                    controller.gameOver = true;
                    controller.notStarted = true;

                    Scores scores = controller.scoreHandler.getScores();
                    Score score = new Score(
                            ScoreType.from(controller.currentLevel, gameResult),
                            controller.components.getMainView().startTime(),
                            controller.components.getMainView().endTime()
                    );

                    List<ScoresGroup> scoresGroups = controller.scoreHandler.calculateScores(score, scores);
                    controller.components.getMainView().showHighscores(scoresGroups);
                })
                .setOnFlagChange(flagsSet -> {
                    controller.components.getMainView().flagsLeft(controller.info.getMines() - flagsSet);
                })
                .setOnFieldsClearedChange(() -> {
                    if (!controller.gameOver) controller.updateFieldsCleared();
                });
    }
}
