package com.szelewicki.minesweeper.controller.main;

import com.szelewicki.minesweeper.component.theme.Themes;
import com.szelewicki.minesweeper.component.api.ComponentActions;
import com.szelewicki.minesweeper.config.AppTranslations;

public class ComponentActionsProvider {

    static ComponentActions provide(MainController mainController) {
        return new ComponentActions() {
            @Override
            public void onWindowClosed() {
                System.out.println(AppTranslations.EXIT_MESSAGE);
                System.exit(0);
            }

            @Override
            public void reset() {
                mainController.resetFields();
            }

            @Override
            public void beginner() {
                mainController.setNewGrid(mainController.settings.beginner());
            }

            @Override
            public void advanced() {
                mainController.setNewGrid(mainController.settings.advanced());
            }

            @Override
            public void expert() {
                mainController.setNewGrid(mainController.settings.expert());
            }

            @Override
            public Themes themes() {
                return mainController.themes;
            }
        };
    }
}
