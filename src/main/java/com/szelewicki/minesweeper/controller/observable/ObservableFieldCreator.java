package com.szelewicki.minesweeper.controller.observable;

import com.szelewicki.minesweeper.commons.Iterations;

public class ObservableFieldCreator {

    public static ObservableField[][] create(int cols, int rows) {
        ObservableField[][] fields = new ObservableField[cols][rows];
        Iterations.forEach(cols, rows, (c, r) -> fields[c][r] = new ObservableField(c, r));
        return fields;
    }
}
