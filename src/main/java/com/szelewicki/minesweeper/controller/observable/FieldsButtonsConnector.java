package com.szelewicki.minesweeper.controller.observable;

import com.szelewicki.minesweeper.commons.Iterations;
import com.szelewicki.minesweeper.component.game.GameButton;

public class FieldsButtonsConnector {

    public static void connect(GameButton[][] buttons, ObservableField[][] fields) {
        Iterations.deepStream(fields)
                .forEach(field -> field.setChangeListener(newState ->
                        buttons[field.getCol()][field.getRow()]
                                .setState(newState.getText(), newState.isDisabled(), newState.getPaint(), newState.getImage())));
    }
}
