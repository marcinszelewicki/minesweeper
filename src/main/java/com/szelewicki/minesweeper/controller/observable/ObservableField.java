package com.szelewicki.minesweeper.controller.observable;

import com.szelewicki.minesweeper.commons.FieldState;

import java.util.Optional;
import java.util.function.Consumer;

public class ObservableField {

    private final int col;
    private final int row;
    private FieldState fieldState;
    private Consumer<FieldState> changeListener;

    public ObservableField(int col, int row) {
        this.col = col;
        this.row = row;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public FieldState getFieldState() {
        return fieldState;
    }

    public void setFieldState(FieldState fieldState) {
        this.fieldState = fieldState;
        Optional.ofNullable(changeListener).ifPresent(listener -> listener.accept(fieldState));
    }

    public void setChangeListener(Consumer<FieldState> changeListener) {
        this.changeListener = changeListener;
    }
}
