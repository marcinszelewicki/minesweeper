package com.szelewicki.minesweeper.controller.highscore;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ScoreCalculator {

    private static final int SCORE_LIST_SIZE_LIMIT = 3;

    public static List<Score> calculateHighscore(Score score, Scores scores) {
        List<Score> previousScores = scores.getScores(score.getType());
        List<Score> newScoresList = new ArrayList<>(previousScores);
        newScoresList.add(score);
        return sort(score, newScoresList);
    }

    private static List<Score> sort(Score score, List<Score> newScoresList) {
        Comparator<Score> comparator = score.getType().isWon() ?
                Comparator.comparingLong(Score::getScoreMillis)
                : Comparator.comparingLong(Score::getScoreMillis).reversed();

        return newScoresList.stream()
                .sorted(comparator)
                .limit(SCORE_LIST_SIZE_LIMIT)
                .collect(Collectors.toList());
    }
}
