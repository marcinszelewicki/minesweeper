package com.szelewicki.minesweeper.controller.highscore;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class ScoresIO {

    public static <T> T readSerializedFile(String fileName, Class<T> serializedClass) {
        try (
                FileInputStream fis = new FileInputStream(fileName);
                GZIPInputStream gis = new GZIPInputStream(fis);
                BufferedInputStream bis = new BufferedInputStream(gis);
                ObjectInputStream inputStream = new ObjectInputStream(bis)
        ) {

            Object object = inputStream.readObject();
            return serializedClass.cast(object);

        } catch (IOException | ClassNotFoundException e) {
            return null;
        }
    }

    public static boolean writeSerializedFile(String fileName, Object object) {
        try (
                FileOutputStream fos = new FileOutputStream(fileName);
                OutputStream os = new GZIPOutputStream(fos);
                ObjectOutputStream outputStream = new ObjectOutputStream(os)
        ) {

            outputStream.writeObject(object);
            return true;

        } catch (IOException e) {
            return false;
        }
    }
}
