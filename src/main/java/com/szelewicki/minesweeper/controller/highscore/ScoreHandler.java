package com.szelewicki.minesweeper.controller.highscore;

import com.szelewicki.minesweeper.commons.SystemInfo;
import com.szelewicki.minesweeper.component.api.ScoresGroup;
import com.szelewicki.minesweeper.component.api.ScoresItem;
import com.szelewicki.minesweeper.config.AppTranslations;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public class ScoreHandler {

    private static final String SCORES_PATH = SystemInfo.resolveAppRoot("highscores");
    private static final String HIGHSCORE_DATE_PATTERN = "dd-MM-uuuu HH:mm";
    private static final String HIGHSCORE_SECONDS_FORMAT = "* %.3f s";

    public Scores getScores() {
        return Optional.ofNullable(ScoresIO.readSerializedFile(SCORES_PATH, Scores.class))
                .orElseGet(Scores::new);
    }

    public void saveScores(Scores scores) {
        ScoresIO.writeSerializedFile(SCORES_PATH, scores);
    }

    public List<ScoresGroup> calculateScores(Score score, Scores scores) {
        calculateAndSave(score, scores);
        return mapToComponentResponse(score, scores);
    }

    private void calculateAndSave(Score score, Scores scores) {
        List<Score> scoreList = ScoreCalculator.calculateHighscore(score, scores);
        scores.putScores(score.getType(), scoreList);
        int scoreIndex = scoreList.indexOf(score);

        if (scoreIndex >= 0) {
            saveScores(scores);
        }
    }

    private List<ScoresGroup> mapToComponentResponse(Score score, Scores scores) {
        String firstTitle = score.getType().isWon() ? AppTranslations.HIGHSCORES_WON : AppTranslations.HIGHSCORES_LOST;
        String secondTitle = score.getType().isWon() ? AppTranslations.HIGHSCORES_LOST : AppTranslations.HIGHSCORES_WON;

        List<Score> firstScores = scores.getScores(score.getType());
        List<Score> secondScores = scores.getScores(score.getType().opposite());

        int firstIndex = firstScores.indexOf(score);
        int secondIndex = secondScores.indexOf(score);

        return Arrays.asList(
                new ScoresGroup(firstTitle, map(firstScores), firstIndex),
                new ScoresGroup(secondTitle, map(secondScores), secondIndex)
        );
    }

    private List<ScoresItem> map(List<Score> firstScores) {
        return firstScores.stream()
                .map(score -> new ScoresItem(formatScore(score.getScoreMillis()), formatDate(score.getDate())))
                .collect(Collectors.toList());
    }

    private String formatDate(LocalDateTime date) {
        return date.format(DateTimeFormatter.ofPattern(HIGHSCORE_DATE_PATTERN));
    }

    private String formatScore(long scoreMillis) {
        return String.format(HIGHSCORE_SECONDS_FORMAT, scoreMillis / 1000.0);
    }
}
