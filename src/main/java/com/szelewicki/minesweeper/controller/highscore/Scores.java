package com.szelewicki.minesweeper.controller.highscore;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Scores implements Serializable {

    private Map<ScoreType, List<Score>> scores = new HashMap<>();

    private void writeObject(ObjectOutputStream stream) throws IOException {
        stream.writeObject(scores);
    }

    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        scores = (Map<ScoreType, List<Score>>) stream.readObject();
    }

    public void putScores(ScoreType type, List<Score> scoreList) {
        scores.put(type, scoreList);
    }

    public List<Score> getScores(ScoreType type) {
        return scores.getOrDefault(type, Collections.emptyList());
    }

    public void setScores(Map<ScoreType, List<Score>> scores) {
        this.scores = scores;
    }
}
