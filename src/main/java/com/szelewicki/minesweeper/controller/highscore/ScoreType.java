package com.szelewicki.minesweeper.controller.highscore;

import java.util.Arrays;

public enum ScoreType {
    BEGINNER_WON(0, true),
    BEGINNER_LOST(0, false),
    ADVANCED_WON(1, true),
    ADVANCED_LOST(1, false),
    EXPERT_WON(2, true),
    EXPERT_LOST(2, false);

    private final int level;
    private final boolean won;

    ScoreType(int level, boolean won) {
        this.level = level;
        this.won = won;
    }

    public int getLevel() {
        return level;
    }

    public boolean isWon() {
        return won;
    }

    public static ScoreType from(int level, boolean won) {
        return Arrays.stream(values())
                .filter(type -> type.level == level && type.won == won)
                .findFirst()
                .orElse(null);
    }

    public ScoreType opposite() {
        return Arrays.stream(values())
                .filter(type -> type.level == this.level && type.won != this.won)
                .findFirst()
                .orElse(null);
    }
}
