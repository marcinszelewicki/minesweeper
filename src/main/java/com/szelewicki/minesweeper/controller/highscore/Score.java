package com.szelewicki.minesweeper.controller.highscore;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;

public class Score implements Serializable {

    private ScoreType type;
    private long startTimeMillis;
    private long endTimeMillis;

    public Score(ScoreType type, long startTimeMillis, long endTimeMillis) {
        this.type = type;
        this.startTimeMillis = startTimeMillis;
        this.endTimeMillis = endTimeMillis;
    }

    private void writeObject(ObjectOutputStream stream) throws IOException {
        stream.writeObject(type);
        stream.writeLong(startTimeMillis);
        stream.writeLong(endTimeMillis);
    }

    private void readObject(ObjectInputStream stream) throws IOException, ClassNotFoundException {
        type = (ScoreType) stream.readObject();
        startTimeMillis = stream.readLong();
        endTimeMillis = stream.readLong();
    }

    public ScoreType getType() {
        return type;
    }

    public long getStartTimeMillis() {
        return startTimeMillis;
    }

    public long getEndTimeMillis() {
        return endTimeMillis;
    }

    public long getScoreMillis() {
        return endTimeMillis - startTimeMillis;
    }

    public LocalDateTime getDate() {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(startTimeMillis), ZoneOffset.systemDefault());
    }
}
