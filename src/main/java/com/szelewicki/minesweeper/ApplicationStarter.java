package com.szelewicki.minesweeper;

import com.szelewicki.minesweeper.component.theme.Themes;
import com.szelewicki.minesweeper.config.DefaultGameProperties;
import com.szelewicki.minesweeper.controller.main.MainController;
import javafx.stage.Stage;

public class ApplicationStarter {

    public void start(Stage primaryStage) {
        new MainController(primaryStage, Themes::randomize)
                .setNewGrid(DefaultGameProperties.advanced());
    }
}
