package com.szelewicki.minesweeper.config;

import com.szelewicki.minesweeper.commons.SystemInfo;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Optional;
import java.util.Properties;

public class AppSettings {

    private static final String SETTINGS_FILE_NAME = "settings";
    private final GamePropertiesInfo beginner;
    private final GamePropertiesInfo advanced;
    private final GamePropertiesInfo expert;

    public AppSettings() {

        Optional<Properties> properties = Optional.ofNullable(getProperties());
        double buttonSize = properties.map(props -> Double.valueOf(props.getProperty("button_size")))
                .orElse(DefaultGameProperties.buttonSize());

        beginner = properties.map(props -> new GamePropertiesInfo(
                0, Integer.parseInt(props.getProperty("beginner_cols")),
                Integer.parseInt(props.getProperty("beginner_rows")),
                Integer.parseInt(props.getProperty("beginner_mines")),
                buttonSize
        ))
                .orElseGet(DefaultGameProperties::beginner);

        advanced = properties.map(props -> new GamePropertiesInfo(
                1, Integer.parseInt(props.getProperty("advanced_cols")),
                Integer.parseInt(props.getProperty("advanced_rows")),
                Integer.parseInt(props.getProperty("advanced_mines")),
                buttonSize
        ))
                .orElseGet(DefaultGameProperties::advanced);

        expert = properties.map(props -> new GamePropertiesInfo(
                2, Integer.parseInt(props.getProperty("expert_cols")),
                Integer.parseInt(props.getProperty("expert_rows")),
                Integer.parseInt(props.getProperty("expert_mines")),
                buttonSize
        ))
                .orElseGet(DefaultGameProperties::expert);
    }

    public GamePropertiesInfo beginner() {
        return beginner;
    }

    public GamePropertiesInfo advanced() {
        return advanced;
    }

    public GamePropertiesInfo expert() {
        return expert;
    }

    private Properties getProperties() {
        Path target = SystemInfo.applicationRootPath().resolve(SETTINGS_FILE_NAME);
        Properties properties = new Properties();

        if (Files.isRegularFile(target)) {
            try (InputStream inputStream = Files.newInputStream(target)) {
                properties.load(inputStream);
                return properties;

            } catch (IOException e) {
                return null;
            }
        }
        return null;
    }
}
