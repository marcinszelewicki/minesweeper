package com.szelewicki.minesweeper.config;

import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

public class DefaultGameProperties {

    private static final Rectangle2D VISUAL_BOUNDS = Screen.getPrimary().getVisualBounds();

    public static double buttonSize() {
        return (int) VISUAL_BOUNDS.getHeight() / 21.0;
    }

    public static double counterFontSize() {
        return 72.0;
    }

    public static double infoFontSize() {
        return 36.0;
    }

    public static double highscoresFontSize() {
        return (int) VISUAL_BOUNDS.getHeight() / 55.0;
    }

    public static GamePropertiesInfo beginner() {
        return new GamePropertiesInfo(0, 9, 9, 10, buttonSize());
    }

    public static GamePropertiesInfo advanced() {
        return new GamePropertiesInfo(1, 16, 16, 40, buttonSize());
    }

    public static GamePropertiesInfo expert() {
        return new GamePropertiesInfo(2, 30, 16, 99, buttonSize());
    }
}
