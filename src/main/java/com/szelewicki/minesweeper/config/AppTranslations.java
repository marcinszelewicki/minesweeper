package com.szelewicki.minesweeper.config;

import javafx.beans.property.SimpleStringProperty;

public class AppTranslations {

    public static final String LEVEL = "Level";
    public static final String THEME = "Theme";
    public static final String INFO = "Info";
    public static final String INFO_VALUE = "Version: 1.1\nkuduacz.pl";

    public static final SimpleStringProperty BEGINNER = new SimpleStringProperty();
    public static final SimpleStringProperty ADVANCED = new SimpleStringProperty();
    public static final SimpleStringProperty EXPERT = new SimpleStringProperty();
    public static final SimpleStringProperty TITLE = new SimpleStringProperty();
    public static final SimpleStringProperty COUNTER_TITLE = new SimpleStringProperty();
    public static final String EXIT_MESSAGE = "bye";
    public static final String HIGHSCORES_WON = "Fastest wins";
    public static final String HIGHSCORES_LOST = "Longest loses";
}
