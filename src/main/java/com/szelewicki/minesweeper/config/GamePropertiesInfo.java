package com.szelewicki.minesweeper.config;

public class GamePropertiesInfo {

    private final int level;
    private final int cols;
    private final int rows;
    private final int mines;
    private final double buttonSize;

    public GamePropertiesInfo(int level, int cols, int rows, int mines, double buttonSize) {
        this.level = level;
        this.cols = cols;
        this.rows = rows;
        this.mines = mines;
        this.buttonSize = buttonSize;
    }

    public int getLevel() {
        return level;
    }

    public int getCols() {
        return cols;
    }

    public int getRows() {
        return rows;
    }

    public int getMines() {
        return mines;
    }

    public double getButtonSize() {
        return buttonSize;
    }
}
