package com.szelewicki.minesweeper.service.api;

public class FieldInfo {

    private final int col;
    private final int row;
    private final boolean mine;
    private final int proximityCount;

    public FieldInfo(int col, int row, boolean mine, int proximityCount) {
        this.col = col;
        this.row = row;
        this.mine = mine;
        this.proximityCount = proximityCount;
    }

    public int getCol() {
        return col;
    }

    public int getRow() {
        return row;
    }

    public boolean isMine() {
        return mine;
    }

    public int getProximityCount() {
        return proximityCount;
    }
}
