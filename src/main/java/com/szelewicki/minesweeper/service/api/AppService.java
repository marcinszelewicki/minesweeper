package com.szelewicki.minesweeper.service.api;

import com.szelewicki.minesweeper.commons.FieldState;
import com.szelewicki.minesweeper.commons.Iterations;
import com.szelewicki.minesweeper.component.resources.Sounds;
import com.szelewicki.minesweeper.controller.observable.ObservableField;
import com.szelewicki.minesweeper.service.fields.EmptyFieldResolver;

import java.util.Set;
import java.util.function.Consumer;

public class AppService {

    private FieldInfo[][] fields;
    private ObservableField[][] observableFields;
    private Consumer<Boolean> onGameOver;
    private Consumer<Integer> flagsSet;
    private Runnable updateFieldsCleared;

    public void setFields(FieldInfo[][] fields) {
        this.fields = fields;
    }

    public void setObservableFields(ObservableField[][] observableFields) {
        this.observableFields = observableFields;
        updateFlagsCount();
        updateFieldsCleared.run();
    }

    public AppService setOnGameOver(Consumer<Boolean> onGameOver) {
        this.onGameOver = onGameOver;
        return this;
    }

    public AppService setOnFlagChange(Consumer<Integer> flagsSet) {
        this.flagsSet = flagsSet;
        return this;
    }

    public AppService setOnFieldsClearedChange(Runnable runnable) {
        this.updateFieldsCleared = runnable;
        return this;
    }

    public void onPrimaryReleased(int col, int row) {
        FieldInfo field = fields[col][row];

        boolean gameOver = true;

        if (!FieldState.ENABLED_FIELDS.contains(observableFields[col][row].getFieldState())) {
            return;
        } else if (field.isMine()) {
            observableFields[col][row].setFieldState(FieldState.MINE);
            resolveMineDiscovered();
            gameOver = false;
        } else if (field.getProximityCount() > 0) {
            observableFields[col][row].setFieldState(FieldState.from(field.getProximityCount()));
        } else {
            EmptyFieldResolver.resolve(col, row, fields, observableFields);
        }
        if (!gameOver || areAllMarked()) {
            onGameOver.accept(gameOver);
        }
        updateFlagsCount();
        updateFieldsCleared.run();
    }

    public void onSecondaryPressed(int col, int row) {

        ObservableField observableField = observableFields[col][row];
        FieldState fieldState = observableField.getFieldState();
        if (fieldState == null || FieldState.ENABLED_FIELDS.contains(fieldState)) {
            switchActiveState(observableField);
            if (areAllMarked()) {
                onGameOver.accept(true);
            }
            Sounds.secondaryPressed();
        }
        updateFlagsCount();
    }

    public void updateFlagsCount() {
        flagsSet.accept(countFlagsSet());
    }

    public int getClearedCount() {
        return Long.valueOf(observableFieldsCount(FieldState.DISABLED_FIELDS)).intValue();
    }

    public long observableFieldsCount(Set<FieldState> fieldState) {
        return Iterations.deepStream(observableFields)
                .map(ObservableField::getFieldState)
                .filter(fieldState::contains)
                .count();
    }

    private void switchActiveState(ObservableField observableField) {

        FieldState fieldState = observableField.getFieldState();
        if (fieldState == null) {
            observableField.setFieldState(FieldState.FLAG);
            return;
        }
        switch (fieldState) {
            case INITIAL:
                observableField.setFieldState(FieldState.FLAG);
                break;
            case FLAG:
                observableField.setFieldState(FieldState.QUESTION);
                break;
            case QUESTION:
                observableField.setFieldState(FieldState.INITIAL);
                break;
        }
    }

    private boolean areAllMarked() {
        return Iterations.deepStream(fields)
                .allMatch(field -> {
                    FieldState fieldState = observableFields[field.getCol()][field.getRow()].getFieldState();
                    return fieldState != null
                            && ((field.isMine() && fieldState == FieldState.FLAG) || fieldState.isDisabled());
                });
    }

    private void resolveMineDiscovered() {
        Iterations.deepStream(fields)
                .forEach(info -> {
                    ObservableField field = observableFields[info.getCol()][info.getRow()];
                    if (info.isMine()) {
                        if (field.getFieldState() != FieldState.FLAG && field.getFieldState() != FieldState.MINE) {
                            field.setFieldState(FieldState.MINE_UNCOVERED);
                        }
                    } else if (field.getFieldState() == FieldState.FLAG) {
                        field.setFieldState(FieldState.FLAG_MISPLACED);
                    }
                });
    }

    private int countFlagsSet() {
        return (int) Iterations.deepStream(observableFields)
                .filter(field -> field.getFieldState() == FieldState.FLAG)
                .limit(Integer.MAX_VALUE)
                .count();
    }
}
