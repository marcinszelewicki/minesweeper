package com.szelewicki.minesweeper.service.fields;

import com.szelewicki.minesweeper.controller.observable.ObservableField;
import com.szelewicki.minesweeper.service.api.FieldInfo;
import com.szelewicki.minesweeper.commons.FieldState;

public class EmptyFieldResolver {

    public static void resolve(int col, int row, FieldInfo[][] fields, ObservableField[][] observableFields) {
        resolveOne(col, row, fields, observableFields);
    }

    private static void checkAdjacent(int col, int row, FieldInfo[][] fields, ObservableField[][] observableFields) {
        resolveOne(col, row - 1, fields, observableFields);
        resolveOne(col, row + 1, fields, observableFields);
        resolveOne(col + 1, row + 1, fields, observableFields);
        resolveOne(col + 1, row, fields, observableFields);
        resolveOne(col + 1, row - 1, fields, observableFields);
        resolveOne(col - 1, row + 1, fields, observableFields);
        resolveOne(col - 1, row, fields, observableFields);
        resolveOne(col - 1, row - 1, fields, observableFields);
    }

    private static void resolveOne(int col, int row, FieldInfo[][] fields, ObservableField[][] observableFields) {
        if (col < 0 || row < 0 || col >= observableFields.length || row >= observableFields[0].length
                || (observableFields[col][row].getFieldState() != null
                && observableFields[col][row].getFieldState().isDisabled())) {
            return;
        }
        if (fieldIsEmpty(col, row, fields)) {
            observableFields[col][row].setFieldState(FieldState.EMPTY);
            checkAdjacent(col, row, fields, observableFields);
        } else {
            int proximityCount = fields[col][row].getProximityCount();
            observableFields[col][row].setFieldState(FieldState.from(proximityCount));
        }
    }

    private static boolean fieldIsEmpty(int col, int row, FieldInfo[][] fields) {
        if (col < 0 || row < 0 || col >= fields.length || row >= fields[0].length) {
            return false;
        }

        return fields[col][row].getProximityCount() == 0;
    }
}
