package com.szelewicki.minesweeper.service.fields;

import com.szelewicki.minesweeper.commons.Iterations;
import com.szelewicki.minesweeper.service.api.FieldInfo;

public class FieldInfoProvider {

    public static FieldInfo[][] provide(boolean[][] mines) {
        FieldInfo[][] fieldInfos = new FieldInfo[mines.length][mines[0].length];

        Iterations.forEach(mines.length, mines[0].length, (col, row) ->
                fieldInfos[col][row] = getField(col, row, mines));

        return fieldInfos;
    }

    private static FieldInfo getField(int col, int row, boolean[][] mines) {

        boolean isMine = mines[col][row];
        int proximityCount = isMine ? 0 : calculateState(col, row, mines);

        return new FieldInfo(col, row, isMine, proximityCount);
    }

    private static int calculateState(int col, int row, boolean[][] mines) {
        int count = 0;
        if (isMine(col - 1, row - 1, mines)) count++;
        if (isMine(col - 1, row, mines)) count++;
        if (isMine(col - 1, row + 1, mines)) count++;
        if (isMine(col, row - 1, mines)) count++;
        if (isMine(col, row + 1, mines)) count++;
        if (isMine(col + 1, row - 1, mines)) count++;
        if (isMine(col + 1, row, mines)) count++;
        if (isMine(col + 1, row + 1, mines)) count++;

        return count;
    }

    private static boolean isMine(int col, int row, boolean[][] mines) {

        if (row < 0 || col < 0 || col >= mines.length || row >= mines[0].length) {
            return false;
        }

        return mines[col][row];
    }
}
