package com.szelewicki.minesweeper.service.fields;

import com.szelewicki.minesweeper.commons.Iterations;

import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

public class MinesRandomizer {

    public static boolean[][] randomize(int cols, int rows, int mineCount, InitiallyEmptyPoints emptyPoints) {

        AtomicInteger counter = new AtomicInteger(0);
        boolean[][] mines = new boolean[cols][rows];

        do {

            Iterations.forEach(0, 0, cols, rows, (col, row) -> {

                if (!mines[col][row] && emptyPoints.isFree(col, row)) {

                    mines[col][row] = isMine(counter, mineCount);

                    if (mines[col][row]) {
                        counter.incrementAndGet();
                    }
                }

                return counter.get() == mineCount;
            });

        } while (counter.get() < mineCount);

        return mines;
    }

    private static boolean isMine(AtomicInteger counter, int mineCount) {
        if (counter.get() == mineCount) {
            return false;
        }
        return new Random().nextInt(100) == 1;
    }
}
