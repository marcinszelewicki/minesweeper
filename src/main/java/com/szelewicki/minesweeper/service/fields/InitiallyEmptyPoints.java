package com.szelewicki.minesweeper.service.fields;

import java.util.HashSet;
import java.util.Set;

public class InitiallyEmptyPoints {

    private final Set<Point> points;

    public InitiallyEmptyPoints(int col, int row) {
        this.points = new HashSet<>();
        points.add(new Point(col + 1, row + 1));
        points.add(new Point(col + 1, row));
        points.add(new Point(col + 1, row - 1));
        points.add(new Point(col, row + 1));
        points.add(new Point(col, row));
        points.add(new Point(col, row - 1));
        points.add(new Point(col - 1, row + 1));
        points.add(new Point(col - 1, row));
        points.add(new Point(col - 1, row - 1));
    }

    public boolean isFree(int col, int row) {
        return points.stream()
                .noneMatch(point -> point.is(col, row));
    }

    private static class Point {
        int col;
        int row;

        private Point(int col, int row) {
            this.col = col;
            this.row = row;
        }

        private boolean is(int col, int row) {
            return this.col == col && this.row == row;
        }
    }
}
