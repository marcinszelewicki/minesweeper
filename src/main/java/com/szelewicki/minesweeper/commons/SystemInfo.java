package com.szelewicki.minesweeper.commons;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SystemInfo {

    private static final String FULL_PATH = SystemInfo.class.getProtectionDomain().getCodeSource().getLocation().getPath();
    private static final int END = FULL_PATH.lastIndexOf(File.separator) + 1;
    private static final String APP_ROOT_PATH = FULL_PATH.substring(0, END);
    private static final Path PATH = Paths.get(decode());

    private static String decode() {
        try {
            return URLDecoder.decode(APP_ROOT_PATH, StandardCharsets.UTF_8.name());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return APP_ROOT_PATH;
    }

    public static Path applicationRootPath() {
        return PATH;
    }

    public static String resolveAppRoot(String relativePath) {
        return PATH.resolve(relativePath).toString();
    }

    public static boolean isWin() {
        return System.getProperty("os.name").toLowerCase().contains("win");
    }
}
