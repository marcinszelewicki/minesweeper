package com.szelewicki.minesweeper.commons;

import java.util.Arrays;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.stream.Stream;

public class Iterations {

    public static void forEach(int iLimit, int jLimit, BiConsumer<Integer, Integer> consumer) {
        for (int i = 0; i < iLimit; i++) {
            for (int j = 0; j < jLimit; j++) {
                consumer.accept(i, j);
            }
        }
    }

    public static void forEach(int iInitial, int jInitial, int iLimit, int jLimit, BiFunction<Integer, Integer, Boolean> function) {
        for (int i = iInitial; i < iLimit; i++) {
            for (int j = jInitial; j < jLimit; j++) {
                boolean breakCondition = function.apply(i, j);
                if (breakCondition) {
                    break;
                }
            }
        }
    }

    public static <T> Stream<T> deepStream(T[][] array) {
        return Arrays.stream(array).flatMap(Arrays::stream);
    }
}
