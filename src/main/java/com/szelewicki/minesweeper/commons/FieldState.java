package com.szelewicki.minesweeper.commons;

import com.szelewicki.minesweeper.component.resources.Images;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;
import javafx.scene.paint.Paint;

import java.util.Arrays;
import java.util.EnumSet;
import java.util.Objects;

public enum FieldState {

    INITIAL("", false, AppColors.BLACK, null),
    FLAG("", false, AppColors.RED, Images.FLAG),
    FLAG_MISPLACED("", false, AppColors.BLACK, Images.FLAG_MISPLACED),
    QUESTION("", false, AppColors.YELLOW, Images.QUESTION),
    MINE("", true, AppColors.WHITE, Images.MINE_FOUND),
    MINE_UNCOVERED("", true, AppColors.BLACK, Images.MINE_NOT_DISCOVERED),
    EMPTY("", true, AppColors.BLACK, null),
    ONE("1", true, AppColors.BLUE, null),
    TWO("2", true, AppColors.GREEN, null),
    THREE("3", true, AppColors.DARK_RED, null),
    FOUR("4", true, AppColors.DARK_BLUE, null),
    FIVE("5", true, AppColors.DARK_GREEN, null),
    SIX("6", true, AppColors.BLACK, null),
    SEVEN("7", true, AppColors.BLACK, null),
    EIGHT("8", true, AppColors.BLACK, null);

    public static final EnumSet<FieldState> ENABLED_FIELDS = EnumSet.of(INITIAL, FLAG, QUESTION);
    public static final EnumSet<FieldState> DISABLED_FIELDS = EnumSet.range(EMPTY, EIGHT);

    private final String text;
    private final boolean disabled;
    private final Paint paint;
    private final SimpleObjectProperty<Image> image;

    FieldState(String text, boolean disabled, AppColors appColor, SimpleObjectProperty<Image> image) {
        this.text = text;
        this.disabled = disabled;
        this.paint = appColor.paint();
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public Paint getPaint() {
        return paint;
    }

    public SimpleObjectProperty<Image> getImage() {
        return image;
    }

    public static FieldState from(int adjacentMineCount) {
        return Arrays.stream(values())
                .filter(state -> Objects.equals(state.getText(), String.valueOf(adjacentMineCount)))
                .findFirst()
                .orElse(EMPTY);
    }
}
