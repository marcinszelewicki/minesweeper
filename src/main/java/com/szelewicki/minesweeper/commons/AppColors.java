package com.szelewicki.minesweeper.commons;

import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public enum AppColors {

    BLACK(Color.BLACK),
    WHITE(Color.WHITE),
    WHITE_OPAQUE(new Color(1.0, 1.0, 1.0, 0.7)),
    RED(new Color(0.9, 0.1, 0.1, 1.0)),
    DARK_RED(Color.DARKRED),
    GREEN(new Color(0.0, 0.7, 0.0, 1.0)),
    DARK_GREEN(new Color(0.0, 0.2, 0.0, 1.0)),
    BLUE(new Color(0.2, 0.2, 1.0, 1.0)),
    DARK_BLUE(new Color(0.0, 0.0, 0.3, 1.0)),
    YELLOW(Color.DARKKHAKI);

    private final Paint paint;

    AppColors(Paint paint) {
        this.paint = paint;
    }

    public Paint paint() {
        return paint;
    }
}
