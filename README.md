# Minesweeper
## Classic game with possibility to create own UI themes 

[![screenshot](https://gitlab.com/marcinszelewicki/minesweeper/raw/master/sweeper.gif)](https://gitlab.com/marcinszelewicki/minesweeper)

Requires Java 1.8+

*[Download executable jar](https://gitlab.com/marcinszelewicki/minesweeper/blob/master/sweeper.jar)*

*[Download settings file (optional)](https://gitlab.com/marcinszelewicki/minesweeper/blob/master/settings)*


## THEMES
* Themes and all associated files are located in `src/main/resources/`

* To add new theme create `.theme` file basing on existing ones.

* Fields can be ignored - default values will be assigned.

> Make sure all theme files are in appropriate folders.


## LICENSE
***This is a non-profit, fan project.***

*All content used in this project including, but not limited to,
text, image, audio, video
is either self-made or used without permission, 
with all rights reserved to respective owners, 
and no challenge to their status intended.*

This software is under [WTFPL](http://www.wtfpl.net/txt/copying/)

[![screenshot](http://www.wtfpl.net/wp-content/uploads/2012/12/logo-220x1601.png)](http://www.wtfpl.net/)

<hr/>
author: kuduacz.pl
<hr/>
